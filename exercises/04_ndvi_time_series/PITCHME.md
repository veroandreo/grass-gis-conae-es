---?image=assets/img/grass.png&position=bottom&size=100% 30%
@title[Front page]

@snap[north span-100]
# Introducción a @color[green](GRASS GIS)
@snapend

@snap[south message-box-white]
Dra. Verónica Andreo<br>CONICET<br><br>
@img[width=600px](assets/img/LogoIG_CONAE_UNC.png)<br>
@snapend

---?image=assets/img/grass.png&position=bottom&size=100% 30%

## Ejercicio: Manos a la obra con series temporales de NDVI 

<br>

+++

@snap[north-west span-60]
### Contenidos
@snapend

@snap[west span-100]
<br><br>
@ol[list-content-verbose]
- Datos para el ejercicio
- Uso de bandas de confiabilidad
- Creación de serie de NDVI
- HANTS como método de reconstrucción
- Agregación temporal
- Índices de fenología
- NDWI y frecuencia de inundación
- Regresión entre NDVI y NDWI
@olend
@snapend

+++

@snap[north span-100]
### Datos para el ejercicio
@snapend

@snap[west span-40]
<br>
@ul[](false)
- Producto MODIS: <a href="https://lpdaac.usgs.gov/products/mod13c2v006/">MOD13C2 Collection 6</a>
- Composiciones globales mensuales 
- Resolución espacial: 5600 m
- Mapset `modis_ndvi` 
@ulend
@snapend

@snap[east span-60]
@img[](assets/img/mod13c2_global_ndvi.png)
@snapend

+++

### Datos para el ejercicio

<br>

- @fa[download text-green] Descargar el mapset [*modis_ndvi*](https://gitlab.com/veroandreo/grass-gis-conae-es/raw/master/data/modis_ndvi.zip?inline=false) y descomprimirlo dentro del location de North Carolina
- @fa[download text-green] Descargar el [código](https://gitlab.com/veroandreo/grass-gis-conae-es/raw/master/code/05_ndvi_time_series_code.sh?inline=false) para seguir el ejercicio

+++

### Preparación de los datos

@code[bash](code/05_ndvi_time_series_code.sh)

@[17-18](Iniciar GRASS GIS en el location NC y crear un nuevo mapset)
@[20-22](Añadir el mapset *modis_lst* a la lista de mapsets accesibles)
@[24-26](Establecer la región a un mapa de LST)
@[28-35](Obtener el *bounding box* en latlong)
@[37-42](Descargar MOD13C2)
@[46-48](Moverse al location latlong e importar los mapas)
@[50-52](Establecer la región a la bounding box obtenida de NC)
@[54-57](Recortar a la región)
@[59-60](Listar mapas que serán reproyectados)
@[64-70](Reproyección - en el location de destino)
@[72-73](Verificar los datos proyectados)

---

### Familiarizarse con los datos de NDVI

@code[bash](code/05_ndvi_time_series_code.sh)

@[85-89](Iniciar GRASS GIS en el mapset *modis_ndvi*)
@[91-93](Agregar *modis_lst* a la lista de mapsets accesibles)
@[95-98](Listar los mapas y obtener información de alguno de ellos)

+++

> @fa[tasks] **Tarea**
> 
> - Mostrar los mapas de NDVI, NIR y pixel reliability.
> - Obtener información sobre los valores mínimos y máximos
> - ¿Qué se puede decir sobre los valores de cada banda?
> - ¿Hay valores nulos?

---

### Uso de la banda de confiabilidad

> @fa[tasks] **Tarea**
>
> - Leer acerca de la banda de confiabilidad en la [Guía de usuario](https://lpdaac.usgs.gov/documents/103/MOD13_User_Guide_V6.pdf) de MOD13 (pag 27).
> - Para una misma fecha mostrar la banda de confiabilidad y el NDVI.
> - Seleccionar sólo los pixeles con valor 0 (Buena calidad) en la banda de confiabilidad. ¿Qué se observa?

+++

### Uso de la banda de confiabilidad

@code[bash](code/05_ndvi_time_series_code.sh)

@[106-108](Definir la región computacional)
@[110-115](Mantener sólo los pixeles de la mejor calidad - *nix)
@[117-121](Mantener sólo los pixeles de la mejor calidad - windows)
@[123-137](Mantener sólo los pixeles de la mejor calidad - para todos los mapas)

+++

> @fa[tasks] **Tarea**
>
> Cómo podrían hacer lo mismo pero con módulos temporales?

<br><br>
@snap[south-east span-100]
<br>
@img[width=90px](assets/img/tip.png) Qué les parece [t.rast.algebra](https://grass.osgeo.org/grass-stable/manuals/t.rast.algebra.html)?
<br><br>
@snapend

+++

```bash
# apply pixel reliability band
t.rast.algebra \
  expression="NDVI_monthly_filt = if(pixel_rel_monthly != 0, null(), ndvi_monthly)"
  basename=ndvi_monthly \
  suffix=gran
```

+++

> @fa[tasks] **Tarea**
>
> Comparar las estadísticas entre los mapas de NDVI originales y filtrados para la misma fecha

---

### Creación de la serie de tiempo

@code[bash](code/05_ndvi_time_series_code.sh)

@[145-149](Crear STRDS)
@[151-152](Corroborar que la STRDS fue creada)
@[154-155](Crear archivo con lista de mapas)
@[157-160](Asignar fecha a los mapas, i.e., registrar)
@[162-163](Imprimir info básica de la STRDS)
@[165-166](Imprimir la lista de mapas en la STRDS)

+++

> @fa[tasks] **Tarea**
> 
> Explorar visualmente los valores de las series temporales en diferentes puntos. 
> Usar [g.gui.tplot](https://grass.osgeo.org/grass-stable/manuals/g.gui.tplot.html) y seleccionar diferentes puntos interactivamente.

---

### Datos faltantes

@code[bash](code/05_ndvi_time_series_code.sh)

@[174-175](Establecer una máscara)
@[177-178](Obtener las estadísticas de la serie de tiempo)
@[180-182](Contar los datos válidos)
@[184-186](Estimar el porcentaje de datos faltantes)

+++

@snap[north span-100]
<br>
@img[width=90px](assets/img/tip.png) Cómo guardar en una variable el número de mapas de una serie de tiempo?
<br><br>
@snapend

```bash
t.info -g ndvi_monthly
`eval t.info ndvi_monthly`
echo $number_of_maps

r.mapcalc \
  expression="ndvi_missing = (($number_of_maps - ndvi_count_valid) * 100.0)/$number_of_maps"
```

+++

> @fa[tasks] **Tarea**
>
> - Mostrar el mapa que representa el porcentaje de datos faltantes y explorar los valores. 
> - Obtener estadísticas univariadas de este mapa.
> - Dónde estan los mayores porcentajes de datos faltantes? Por qué creen que puede ser?

---

### Reconstrucción temporal: HANTS

- Harmonic Analysis of Time Series (HANTS)
- Implementado en la extensión [r.hants](https://grass.osgeo.org/grass7/manuals/addons/r.hants.html)

<img src="assets/img/evi_evi_hants.png" width="65%">

+++

### Reconstrucción temporal: HANTS

@code[bash](code/05_ndvi_time_series_code.sh)

@[194-195](Instalar la extensión *r.hants*)
@[197-202](Listar los mapas y aplicar r.hants - *nix)
@[204-208](Listar los mapas y aplicar r.hants - windows)

+++

> @fa[tasks] **Tarea**
>
> Probar diferentes ajustes de parámetros en [r.hants](https://grass.osgeo.org/grass7/manuals/addons/r.hants.html) y comparar los resultados

+++

### Reconstrucción temporal: HANTS

@code[bash](code/05_ndvi_time_series_code.sh)

@[210-215](Parcheo de serie original y reconstruída - *nix)
@[217-222](Parcheo de serie original y reconstruída - windows)
@[224-236](Parcheo de serie original y reconstruída)
@[238-242](Crear serie de tiempo con los datos parcheados)
@[244-250](Registrar los mapas en la serie de tiempo)
@[252-253](Imprimir información de la serie de tiempo)

+++

> @fa[tasks] **Tarea**
>
> - Evaluar gráficamente los resultados de la reconstrucción de HANTS en pixeles con mayor porcentaje de datos faltantes 
> - Obtener estadísticas univariadas para las nuevas series temporales

+++

> @fa[tasks] **Tarea**
>
> - Ver la sección de métodos en [Metz el al. 2017](https://www.mdpi.com/2072-4292/9/12/1333) 
> - Qué otros algoritmos existen o qué otra aproximación podría seguirse?

@img[span-60](https://www.mdpi.com/remotesensing/remotesensing-09-01333/article_deploy/html/images/remotesensing-09-01333-ag.png)

---

### Agregación con granularidad

<br>

> @fa[tasks] **Tarea**
>
> - Obtener el promedio de NDVI cada dos meses
> - Visualizar la serie de tiempo resultante con [g.gui.animation](https://grass.osgeo.org/grass-stable/manuals/g.gui.animation.html)

---

### Fenología

@code[bash](code/05_ndvi_time_series_code.sh)

@[261-263](Mes de máximo y mes de mínimo)
@[265-272](Reemplazar con *start_month()* los valores en la STRDS si coinciden con el mínimo o máximo global)
@[274-276](Obtener el primer mes en el que aparecieron el máximo y el mínimo)
@[278-279](Eliminar la serie temporal intermedia)

+++

> @fa[tasks] **Tarea**
>
> - Mostrar los mapas resultantes con [g.gui.mapswipe](https://grass.osgeo.org/grass-stable/manuals/g.gui.mapswipe.html)
> - Cuándo se observan los mínimos y máximos? Hay algun patrón? A qué se podria deber?

+++

> @fa[tasks] **Tarea**
>
> Asociar el máximo de LST con el máximo de NDVI y, la fecha de la máxima LST con la fecha del máximo NDVI

<br>
@snap[south-east span-70]
@img[width=90px](assets/img/tip.png) Ver el módulo [r.covar](https://grass.osgeo.org/grass-stable/manuals/r.covar.html)
<br><br>
@snapend

+++

### Fenología

@code[bash](code/05_ndvi_time_series_code.sh)

@[281-284](Obtener series temporales de pendientes entre mapas consecutivos)
@[286-289](Obtener la máxima pendiente por año)

+++

> @fa[tasks] **Tarea**
>
> - Obtener un mapa con la mayor tasa de crecimiento por pixel en el período 2015-2019
> - Qué modulo usarían?

+++

### Fenología

@code[bash](code/05_ndvi_time_series_code.sh)

@[291-292](Instalar la extensión *r.seasons*)
@[294-297](Determinar el comienzo, el final y la duración del período de crecimiento - *nix)
@[299-302](Determinar el comienzo, el final y la duración del período de crecimiento - windows)

+++

> @fa[tasks] **Tarea**
>
> - Qué nos dice cada mapa? Dónde es más larga la estacion de crecimiento?
> - Exportar los mapas resultantes como .png

+++

### Fenología

@code[bash](code/05_ndvi_time_series_code.sh)

@[304-305](Crear un mapa de umbrales para usar en *r.seasons*)

+++

> @fa[tasks] **Tarea**
>
> Utilizar el mapa de umbrales en [r.seasons](https://grass.osgeo.org/grass7/manuals/addons/r.seasons.html) y comparar los mapas de salida con los resultados de utilizar sólo un valor de umbral

---

### Serie de tiempo de NDWI

@code[bash](code/05_ndvi_time_series_code.sh)

@[313-322](Crear series temporales de NIR y MIR)
@[324-326](Listar archivos NIR y MIR)
@[328-335](Registrar mapas)
@[337-339](Imprimir información de la serie de tiempo)
@[341-343](Estimación de la serie temporal de NDWI)

+++

> @fa[tasks] **Tarea**
>
> Obtener valores máximos y mínimos para cada mapa de NDWI y explorar el trazado de la serie de tiempo en diferentes puntos de forma interactiva

<br><br>

@snap[south-east span-70]
@img[width=90px](assets/img/tip.png) Ver el manual de [t.rast.univar](https://grass.osgeo.org/grass-stable/manuals/t.rast.univar.html)
<br><br>
@snapend

---

### Frecuencia de inundación

@code[bash](code/05_ndvi_time_series_code.sh)

@[351-353](Reclasificar los mapas según un umbral)
@[355-356](Obtener frecuencia de inundación)

+++

> @fa[tasks] **Tarea**
>
> Cuáles son las áreas que se han inundado con más frecuencia?

---

### Regresión

@code[bash](code/05_ndvi_time_series_code.sh)

@[364-365](Instalar la extensión *r.regression.series*)
@[367-372](Realizar una regresión entre las series temporales de NDVI y NDWI - *nix)
@[374-379](Realizar una regresión entre las series temporales de NDVI y NDWI - windows)

+++

> @fa[tasks] **Tarea**
>
> Determinar dónde está la mayor correlación entre NDVI y NDWI

---

<img src="assets/img/gummy-question.png" width="45%">

---

**Gracias por su atención!!**

![GRASS GIS logo](assets/img/grass_logo_alphab.png)

---

## THE END! 

<br>
@fa[grin-beam-sweat fa-3x fa-spin text-green]


@snap[south span-50]
@size[18px](Presentation powered by)
<br>
<a href="https://gitpitch.com/">
<img src="assets/img/gitpitch_logo.png" width="20%"></a>
@snapend
