# Introducción a GRASS GIS

Curso online de introducción a GRASS GIS en español basado en los datos de muestra de North Carolina. Este curso es igual al que se encuentra en el repo [grass-gis-conae](https://gitlab.com/veroandreo/grass-gis-conae) sólo que ha sido traducido al español.

## Presentaciones y ejercicios

### Presentaciones:

- [Breve introducción a GRASS GIS](https://gitlab.com/veroandreo/grass-gis-conae-es/-/blob/master/pdf/01_intro.pdf)
- [Un paseo por las funciones de GRASS GIS](https://gitlab.com/veroandreo/grass-gis-conae-es/-/blob/master/pdf/02_intro_general_capabilities.pdf)
- [Procesamiento de datos raster en GRASS GIS](https://gitlab.com/veroandreo/grass-gis-conae-es/-/blob/master/pdf/03_raster.pdf)
- [Procesamiento de datos satelitales en GRASS GIS](https://gitlab.com/veroandreo/grass-gis-conae-es/-/blob/master/pdf/04_imagery.pdf)
- [Procesamiento de series de tiempo en GRASS GIS](https://gitlab.com/veroandreo/grass-gis-conae-es/-/blob/master/pdf/05_time_series.pdf)

### Ejercicios:

- [Familiarizándonos con GRASS GIS](https://gitlab.com/veroandreo/grass-gis-conae-es/-/blob/master/pdf/01_exercise_getting_familiar.pdf)
- [Crear un location e importar mapas a GRASS GIS](https://gitlab.com/veroandreo/grass-gis-conae-es/-/blob/master/pdf/02_exercise_create_new_location.pdf)
- [Trabajamos con imágenes Sentinel 2](https://gitlab.com/veroandreo/grass-gis-conae-es/-/blob/master/pdf/03_exercise_processing_sentinel2.pdf)
- [Manos a la obra con series temporales de NDVI](https://gitlab.com/veroandreo/grass-gis-conae-es/-/blob/master/pdf/04_exercise_ndvi_time_series.pdf)

## Software

Para el desarrollo de este curso se utilizará **GRASS GIS 7.8.3**, que es
la versión estable actual. Se la puede instalar a través de sus diversos 
binarios o ejecutables o a través de OSGeo-Live que incluye todos los demás 
paquetes de software de OSGeo. Se recomienda ver la presentación 
[**Guía de instalación**](https://gitlab.com/veroandreo/grass-gis-conae-es/-/blob/master/pdf/00_installation.pdf) 
para más detalles. No obstante, aquí abajo se muestran los links para
los sistemas mas comúnmente usados.

### Instaladores para los sistemas operativos mas comunes:

##### MS Windows

Existen dos opciones diferentes para los usuarios de MS Windows:
1. Ejecutable con todas las dependencias: [32-bit](https://grass.osgeo.org/grass78/binary/mswindows/native/x86/WinGRASS-7.8.3-1-Setup-x86.exe) | [64-bit](https://grass.osgeo.org/grass78/binary/mswindows/native/x86_64/WinGRASS-7.8.3-1-Setup-x86_64.exe) 
2. Paquete OSGeo4W: [32-bit](http://download.osgeo.org/osgeo4w/osgeo4w-setup-x86.exe) | [64-bit](http://download.osgeo.org/osgeo4w/osgeo4w-setup-x86_64.exe) 

Se recomienda optar por la segunda opción, **OSGeo4W**, ya que permite
instalar todos los programas y paquetes de OSGeo. Al elegir esta opción
asegurarse de de seleccionar **GRASS GIS** y **msys**. Este último, permitirá 
usar algunos trucos disponibles normalmente en las terminales *bash*, tales
como ciclos, back tick, autocompletado, historia de comandos, entre otros. 

##### Ubuntu Linux

GRASS GIS 7.8.3 se puede instalar usando el repositorio "unstable":

```
sudo add-apt-repository ppa:ubuntugis/ubuntugis-unstable
sudo apt-get update
sudo apt-get install grass grass-dev grass-gui
```

##### Fedora, openSuSe y otras distros

Para otras distribuciones de Linux, incluyendo **Fedora** y **openSuSe**, 
simplemente instalar GRASS GIS a partir de los repositorios oficiales. 
Ver también el siguiente [enlace](https://grass.osgeo.org/download/)
para más detalles.

```
sudo dnf install grass grass-gui grass-devel
```

##### Otras dependencias

Las siguientes son librerías de Python requeridas por las extensiones que
usaremos durante el curso:
- [pyModis](http://www.pymodis.org): librería para trabajar con datos MODIS. Ofrece descarga, mosaico, re-proyección, conversión de formato y extracción de bandas de calidad. Esta librería es necesaria para *[i.modis](https://grass.osgeo.org/grass7/manuals/addons/i.modis.html)*.
- [sentinelsat](https://github.com/sentinelsat/sentinelsat): utilidad para búsqueda y descarga de imágenes Copernicus Sentinel del [Copernicus Open Access Hub](https://scihub.copernicus.eu/). Esta librería es necesaria para *[i.sentinel](https://grass.osgeo.org/grass7/manuals/addons/i.sentinel.html)*.
- [scikit-learn](https://scikit-learn.org/stable/): librería de python para machine learning. Es requerida por la extensión *[r.learn.ml](https://grass.osgeo.org/grass7/manuals/addons/r.learn.ml.html)*.

Ver la [**Guía de instalación**](https://gitlab.com/veroandreo/grass-gis-conae-es/-/blob/master/pdf/00_installation.pdf)
para más detalles.

### OSGeo-live: 

[OSGeo-Live](https://live.osgeo.org/) es un sistema booteable auto-contenido basado 
en Lubuntu, que puede cargarse en un USB o una máquina virtual, y que permite probar
una amplia variedad de software geo-espacial sin necesidad de instalar nada.
Existen diferentes opciones para ejecutar OSGeo-Live:

* [Ejecutar OSGeo-Live en una Máquina Virtual](https://live.osgeo.org/en/quickstart/virtualization_quickstart.html)
* [Ejecutar OSGeo-Live desde un USB](https://live.osgeo.org/en/quickstart/usb_quickstart.html)

Para una guía rápida, ver [aquí](https://live.osgeo.org/en/quickstart/osgeolive_quickstart.html).

### Extensiones de GRASS GIS que utilizaremos durante el curso

* [i.sentinel](https://grass.osgeo.org/grass7/manuals/addons/i.sentinel.html): Herramientas para descargar y procesar productos Copernicus Sentinel
* [i.fusion.hpf](https://grass.osgeo.org/grass7/manuals/addons/i.fusion.hpf.html): Fusión de datos pancromáticos de alta resolución y multiespectrales de baja resolución basados en la técnica de adición de filtros de paso alto
* [i.landsat8.qc](https://grass.osgeo.org/grass7/manuals/addons/i.landsat8.qc.html): Reclasifica la banda QA de Landsat 8 según la calidad de los píxeles
* [i.wi](https://grass.osgeo.org/grass7/manuals/addons/i.wi.html): Calcula diferentes tipos de índices de agua
* [i.superpixels.slic](https://grass.osgeo.org/grass7/manuals/addons/i.superpixels.slic.html): Realiza la segmentación de una imagen utilizando el método de segmentación SLIC
* [i.modis](https://grass.osgeo.org/grass7/manuals/addons/i.modis.html): Conjunto de herramientas para descargar y procesar productos MODIS utilizando pyModis
* [r.hants](https://grass.osgeo.org/grass7/manuals/addons/r.hants.html): Aproxima una serie temporal periódica y crea una salida reconstruída
* [r.seasons](https://grass.osgeo.org/grass7/manuals/addons/r.seasons.html): Extrae las estaciones a partir de una serie de tiempo
* [r.regression.series](https://grass.osgeo.org/grass7/manuals/addons/r.regression.series.html): Calcula los parámetros de regresión lineal entre dos series temporales
* [v.strds.stats](https://grass.osgeo.org/grass7/manuals/addons/v.strds.stats.html): Estadísticas zonales de determinados conjuntos de datos raster espacio-temporales basados en un mapa vectorial de polígonos
* [r.learn.ml](https://grass.osgeo.org/grass7/manuals/addons/r.learn.ml.html): Clasificación supervisada y regresión de mapas raster usando el paquete scikit-learn de python

Instalar con: `g.extension extension=nombredelaextension`

## Datos

* [Location North Carolina (dataset completo, 150Mb)](https://grass.osgeo.org/sampledata/north_carolina/nc_spm_08_grass7.zip): descomprimir en `$HOME/grassdata`
* [Mapset modis_lst (2Mb)](https://gitlab.com/veroandreo/grass-gis-conae-es/raw/master/data/modis_lst.zip?inline=false): descomprimir dentro del location North Carolina `$HOME/grassdata/nc_spm_08_grass7` 
* [Mapset modis_ndvi (15 Mb)](https://gitlab.com/veroandreo/grass-gis-conae-es/raw/master/data/modis_ndvi.zip?inline=false): descomprimir dentro del location North Carolina `$HOME/grassdata/nc_spm_08_grass7`
* [Mapa raster (1Mb)](https://gitlab.com/veroandreo/grass-gis-conae-es/raw/3b11ad06d2133889e0ee51652a03f94bfec9d7e4/data/sample_rasters.zip?inline=false): descargar y mover a `$HOME/gisdata`
* [Mapa vectorial (3Mb)](https://gitlab.com/veroandreo/grass-gis-conae-es/raw/master/data/streets.gpkg?inline=false): descargar y mover a `$HOME/gisdata`
* [Escena Landsat 8 (14Mb)](https://gitlab.com/veroandreo/grass-gis-conae-es/raw/master/data/NC_L8_scenes.zip?inline=false): descargar y descomprimir dentro de `$HOME/gisdata`
* [Escena Sentinel 2 (500Mb)](https://www.dropbox.com/s/2k8wg9i05mqgnf1/S2A_MSIL1C_20180822T155901_N0206_R097_T17SQV_20180822T212023.zip?dl=0): descargar y mover a `$HOME/gisdata`. *NO DESCOMPRIMIR*
* [Aeronet AOD (12Kb)](https://gitlab.com/veroandreo/grass-gis-conae-es/raw/master/data/180819_180825_EPA-Res_Triangle_Pk.zip?inline=false): descargar y descomprimir dentro de `$HOME/gisdata`

## Sobre la disertante

**Verónica Andreo** es investigadora asistente de [CONICET](http://www.conicet.gov.ar/?lan=en)
y desarrolla sus actividades en el **Instituto de Altos Estudios Espaciales "Mario Gulich"** 
[(IG)](https://ig.conae.unc.edu.ar/) dependiente de la CONAE y la UNC. Su 
investigación se enfoca en las aplicaciones del sensado remoto y los sistemas de 
información geográfica (SIG) en problemas relacionados con la Salud Pública y los huéspedes
y vectores de enfermedades zoonóticas.  
Vero es Charter member de [OSGeo](http://www.osgeo.org/) y ferviente promotora de 
[FOSS4G](http://foss4g.org/). Además, es parte del 
[GRASS GIS Development team](https://grass.osgeo.org/home/credits/) 
y dicta cursos y talleres introductorios y avanzados sobre los módulos temporales de 
GRASS GIS y sus aplicaciones. Para más detalles, visitar: **https://veroandreo.gitlab.io/**.

## Licencias

Las presentaciones fueron creadas con [gitpitch](https://gitpitch.com/) bajo la licencia **MIT**. 
Todo el material del curso está bajo licencia **Creative Commons Attribution-ShareAlike 4.0 International**.

[![Creative Commons](assets/img/ccbysa.png)](http://creativecommons.org/licenses/by-sa/4.0/) 
